# Distribution exercises

Using numpy, pandas and scipy.stats module I research about probability concepts such as probabilities, different kinds of distributions and their cases of uses.

## Homework 🩹
There are a doc with 23 exercises; each one in notebook in numbered , all around indetify the correct function to use regarding the kind of distribution.

## Distributions 🍋
Regarding if data values are continuous or discrete kind distribution are different:

```markdown
    Continous:
    |
    |-Normal
    |-Exponential
    |-Weibul

    Discrete:
    |
    |-Binomial
    |-Poisson
    |-Hyper-geometric
```
Enjoy ~ :bamboo:

